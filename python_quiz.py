#!/usr/bin/env python3
# Docs1: https://www.tutorialspoint.com/pyqt/pyqt_basic_widgets.htm
# Docs2: https://wiki.qt.io/Category:Snippets
# Docs3: http://pyqt.sourceforge.net/Docs/PyQt4/classes.html

import sys, random
from PyQt4.QtGui import *
from PyQt4.QtCore import *

class MainWindow(QMainWindow):
    def __init__(self, parent = None):
        super(MainWindow, self).__init__(parent)
        self.setWindowTitle('Python Quiz')
        self.setWindowIcon(QIcon('icon.png'))

        global vragen
        # Insert items as: <question>,<answer>
        vragen = [] # vragen translates to questions (Dutch > English)

        # Static values for debug
        #vragen = ['1 == 1 and 1 != 1,false', '1 == 1 or 1 != 1,true', 'Translate: hallo,hallo']

        # Import from list
        try:
            list = open('./questions.csv').readlines()
            for question in list:
                # Set '#' on the beginning to make them a comment in the questions CSV file
                if not question.startswith('#'):
                    vragen.append(question)
        except Exception as e:
            error = QMessageBox.critical(self, 'Error', '%s' % e, QMessageBox.Ok)
            sys.exit(1)

        # Roll for a question
        def get_question():
            global vraag
            if not len(vragen) == 0:
                vraag = random.choice(vragen) # Vraag translates to question (Dutch > English)
                self.setWindowTitle('Remaining questions: ' + str(len(vragen))) # must be a str for window title, so I did str(len(vragen))
                vragen.remove(vraag) # Remove question from the list

                self.question.setText(vraag.split(',')[0])
                self.answer_field.setText('')
            else:
                #finished = QMessageBox.information(self, "Finished","You finished the test.\nYour result is %i correct and %i wrong answers" % (0,0), QMessageBox.Ok)
                finished = QMessageBox.information(self, "Finished","You finished the test. Well done!", QMessageBox.Ok) # Not tracking correct/wrong answers
                sys.exit(0)



        # Check if the given answer is correct
        def check_answer():
            a = self.answer_field.text()
            # If not input given
            if not a:
                error = QMessageBox.critical(self, "Error", "Please, answer the question")
                return
                self.answer_field.setText('') # Clear input field
            else:
                pass

            # Display message box based on the result
            if not self.answer_field.text().lower().strip('!@#%*()_+{}|~`:;,./?¡²³¤¼½¾‘’¥×¿') in vraag.split(',')[1].lower(): # Strip special characters from the given input
                mistakes_happen = QMessageBox.critical(self, "Wrong answer","Answer is: %s" % vraag.split(',')[1], QMessageBox.Ok)
                #print(self.answer_field.text()) # Debug
                #print(vraag.split(',')[1]) # Debug
                self.answer_field.setText('') # Clear input field
            else:
                keep_going = QMessageBox.information(self, "Correct answer","Correct!", QMessageBox.Ok)
                self.answer_field.setText('') # Clear input field
                get_question()

        def skip_question():
            get_question()

        def show_answers():
            f2 = QDialog()
            f2.setWindowTitle('Test answers')
            f2.setWindowModality(Qt.ApplicationModal)

            # Close button
            c = QPushButton('OK', f2)
            c.setToolTip('Close window')
            c.setShortcut('Return')
            c.clicked.connect(f2.close)

            f2.exec_() # Show dialog

        # Exit button
        self.exit_button = QPushButton('Exit', self)
        self.exit_button.setToolTip('Click to exit application')
        self.exit_button.clicked.connect(exit)
        self.exit_button.setShortcut('Esc')
        self.exit_button.move(0,300)
        self.exit_button.setFixedWidth(100)
        self.exit_button.setFixedHeight(50)

        # Show answers button
        self.answers_button = QPushButton('Show Answers', self)
        self.answers_button.setToolTip('Show the answers to all questions')
        self.answers_button.clicked.connect(show_answers)
        self.answers_button.setShortcut('Backspace')
        self.answers_button.move(0,250)
        self.answers_button.setFixedWidth(100)
        self.answers_button.setFixedHeight(50)

        # Skip question button
        self.skip_button = QPushButton('Skip', self)
        self.skip_button.setToolTip('Skip question')
        self.skip_button.clicked.connect(skip_question)
        self.skip_button.setShortcut('S')
        self.skip_button.move(250,250)
        self.skip_button.setFixedWidth(100)
        self.skip_button.setFixedHeight(50)

        # Next button
        self.next_button = QPushButton('Next >', self)
        self.next_button.setToolTip('Next question')
        self.next_button.clicked.connect(check_answer)
        self.next_button.setShortcut('Return')
        self.next_button.move(500,250)
        self.next_button.setFixedWidth(100)
        self.next_button.setFixedHeight(50)

        # answer label
        self.answer_label = QLabel('Answer', self)
        self.answer_label.move(20, 200)
        # answer input field
        self.answer_field = QLineEdit(self)
        self.answer_field.move(100, 200)
        self.answer_field.setFixedWidth(500)
        self.answer_field.setFocus() # Give focus

        # question label
        self.question = QLabel('If you see this, something went wrong and the script failed to display a question from the imported list.', self)
        self.question.setFixedWidth(580)
        self.question.setFixedHeight(150)
        self.question.setStyleSheet("border: 3px solid blue;")
        self.question.setAlignment(Qt.AlignLeft) # Text Alignment (default: center)
        self.question.move(20, 50) # Label position
        self.question.setWordWrap(True) # Enable word wrap

        get_question() # Roll for a question

        # total questions
        t = len(vragen) + 1

        # Question
        if t > 1:
            welcome = QMessageBox.information(self, "Starting","This test has %i questions, good luck!" % t, QMessageBox.Ok | QMessageBox.Cancel)
        else:
            # QuestionS, for perfection
            welcome = QMessageBox.information(self, "Starting","This test has %i question, good luck!" % t, QMessageBox.Ok | QMessageBox.Cancel)
        if welcome == QMessageBox.Cancel:
            sys.exit(0)


def main():
   app = QApplication(sys.argv)
   ex = MainWindow()
   ex.resize(620,350) # Window size
   ex.show() # Show MainWindow()
   sys.exit(app.exec_())

# Start app
if __name__ == '__main__':
   main()
